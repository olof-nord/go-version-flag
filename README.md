# go-version-flag-demo

Demo project for how to implement a version flag for a golang program

## Dependencies

This demo requires `go` to be installed locally.

## Build and Run

To build the program with go directly:
`go build .`

To run the built application:

```shell
$ ./go-version-flag-demo
  -v, --version
    	Print version info and exit
```

Output when using the version flag:

```shell
$ ./go-version-flag-demo --version
go-version-flag-demo 1.0.0 linux amd64 go1.15.8
```

Repository icon made by [Freepik](https://www.freepik.com) from [flaticon.com](https://www.flaticon.com).
