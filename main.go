package main

import (
    "fmt"
    "flag"
    "os"
    "runtime"

    "rsc.io/getopt"

    "gitlab.com/olof-nord/go-version-flag-demo/config"
)

func init() {
    flag.BoolVar(&config.PrintVersion, "version", false, "Print version info and exit")
    getopt.Alias("v", "version")
}

func main() {
    getopt.Parse()

    if config.PrintVersion {
        fmt.Printf("%s %s %s %s %s\n",
        			config.AppName, config.AppVersion, runtime.GOOS, runtime.GOARCH, runtime.Version())
        os.Exit(0)
    }

    getopt.PrintDefaults()
}
